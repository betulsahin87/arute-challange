public class Task {
	
	public Task() {
		Console.WriteLine("Task class's constructor worked..");
	}
	
	public sealed int Id { get; set; }
	
	protected sealed void Find() {
		Console.WriteLine("find method worked..");
	}
	
	protected sealed void Save() {
		Console.WriteLine("save method worked..");
	}
	
	protected sealed void Delete() {
		Console.WriteLine("delete method worked..");
	}

}